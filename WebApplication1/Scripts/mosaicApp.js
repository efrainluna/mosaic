﻿// Define the `mosaicApp` module
var mosaicApp = angular.module('mosaicApp', []);

// Define the `TestController` controller on the `mosaicApp` module
mosaicApp.controller('testController', function($scope) {
    $scope.tasks = [];
    $scope.description = "";

    $scope.addTask = function () {
        if ($scope.description != undefined && $scope.description != "") {
            $scope.tasks.unshift({ 'done': false, 'descripcion': $scope.description });
            $scope.description = "";
        } else {
            alert("You have to wirte some task");
        }
        
    };

    $scope.removeTask = function (index) {
        $scope.tasks.splice(index, 1);
    };
});